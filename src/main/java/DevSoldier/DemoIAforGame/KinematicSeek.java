package DevSoldier.DemoIAforGame;

import java.util.Objects;

public class KinematicSeek {
    Kinematic character;
    Kinematic target;
    float maxSpeed;

    public KinematicSeek() {
    }

    public KinematicSeek(Kinematic charater, Kinematic target, float maxSpeed) {
        this.character = charater;
        this.target = target;
        this.maxSpeed = maxSpeed;
    }

    public Kinematic getCharater() {
        return character;
    }

    public Kinematic getTarget() {
        return target;
    }

    public float getMaxSpeed() {
        return maxSpeed;
    }

    public void setCharater(Kinematic charater) {
        this.character = charater;
    }

    public void setTarget(Kinematic target) {
        this.target = target;
    }

    public void setMaxSpeed(float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KinematicSeek other = (KinematicSeek) obj;
        if (Float.floatToIntBits(this.maxSpeed) != Float.floatToIntBits(other.maxSpeed)) {
            return false;
        }
        if (!Objects.equals(this.character, other.character)) {
            return false;
        }
        if (!Objects.equals(this.target, other.target)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "KinematicSeek{" + "charater=" + character + ", target=" + target + ", maxSpeed=" + maxSpeed + '}';
    }
    
    public KinematicOutput ganerateKinematicOutput(){
        Vector2D velocity = new Vector2D();
        velocity = character.getPosition().subVector2D(target.getPosition());
        velocity.normalize();
        velocity.mulConstant(maxSpeed);
        return new KinematicOutput(velocity,0);
    }
}
